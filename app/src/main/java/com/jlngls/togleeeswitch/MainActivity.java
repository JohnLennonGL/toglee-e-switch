package com.jlngls.togleeeswitch;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity {

    private ToggleButton toggle;
    private Switch aSwitch;
    private CheckBox checkBox;
    private RadioGroup radioGroup;
    private RadioButton radioLigado;
    private TextView resultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        resultado   = findViewById(R.id.resultadoID);
        toggle      = findViewById(R.id.toggleID);
        aSwitch     = findViewById(R.id.switchID);
        checkBox    = findViewById(R.id.checkBoxID);
        adicionarOuvinte();

        // radio button
//        radioGroup  = findViewById(R.id.radioGroup);
//        radioLigado = findViewById(R.id.radioLigado);
//
//        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(RadioGroup group, int checkedId) {
//                if (radioLigado.isChecked()) {
//                    resultado.setText("ligado");
//                } else {
//                    resultado.setText("Desligado");
//                }
//            }
//        });

    }

    // metodo correto
    public void adicionarOuvinte(){
        //toggle
        //checkBox
        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

              if(isChecked){
                  resultado.setText("ligado");
              }
              else{
                  resultado.setText("Desligado");
              }
            }
        });
    }

    /// metodo gambiarra
    public void onClick(View view) {
        if (
               checkBox.isChecked()
            // toggle.isChecked()
            // aSwitch.isChecked()
        ) {

            resultado.setText("ligado");
        } else {
            resultado.setText("Desligado");
        }
    }
}
